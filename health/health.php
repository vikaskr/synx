<!DOCTYPE HTML>
<html>
<head>
	<title>Motive Group : We are commited for Better Service</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!-- style -->
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="fi/flaticon.css">
	<link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="css/animate.css">
	<!--styles -->
</head>
<body>
	<!-- page header -->
	<header class="only-color">
		<!-- header top panel -->
		
		<!-- / header top panel -->
		<!-- sticky menu -->
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<!-- logo -->
					<a href="../index.html" class="logo">
						<img src="img/2.jpg" alt="logo" class="logo" style="width: 170px; height: 100px; margin-top: -1em;">
					</a>
					<!-- / logo -->
					<nav class="main-nav">
						<ul class="clear-fix">
						    <li class="static"><a href="../index.html" class="dropdown-toggle" data-toggle="dropdown">Home</a></li>
	                        <li><a class="scrollings active" href="#health">Health-Care</a></li>
	                        <li><a class="scrollings" href="#home">Services</a></li>
	                        <li><a class="scrollings" href="#how" class="scrollings">How We Work</a></li>
	                        <li><a class="scrollings" href="#about" class="scrollings">About Motive Group</a></li>
	                        <li><a href="#footer" class="scrollings">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- sticky menu -->
	</header>
	<!-- / page header -->
	
	<div class="">
		<div class="">
			<img src="../images/health.jpg" data-at2x="../images/health.jpg" alt style="height: 580px!important; width: 100%!important;">
			<div class="container" style="margin-top: -20em; margin-bottom: 11em;">
				<div class="rows">
					<p style="font-size: 40px; line-height: 50px; padding: 14px; font-weight: 500;">WE ARE COMMITTED <br/>FOR BETTER SERVICES </p>
				</div>
			</div>
		</div>
	</div>
	<hr class="divider-color">
	<!-- content -->
	<div id="home" class="page-content padding-none">
		<!-- section -->
		<section class="padding-section">
			<div class="grid-row clear-fix">
				<h2 class="center-text">Health Care Services</h2>
				<div class="grid-col-row">
					<div class="grid-col grid-col-4">
						<!-- course item -->
						<div class="course-item">
							<div class="course-hover">
								<img src="../images/h1.jpg" data-at2x="../images/h1.jpg" alt>
								
							</div>
					
							<div class="course-date bg-color-1 clear-fix">
								
								<div class="divider"></div>
								<div class="description">High quality health and care delivery</div>
							</div>
						</div>
						<!-- / course item -->
					</div>
					<div class="grid-col grid-col-4">
						<!-- course item -->
						<div class="course-item">
							<div class="course-hover">
								<img src="../images/h2.jpg" data-at2x="../images/h2.jpg" alt="">
								
							</div>
							<div class="course-date bg-color-2 clear-fix">
								
								<div class="divider"></div>
								<div class="description">Population health and well-being</div>
							</div>
						</div>
						<!-- / course item -->
					</div>
					<div class="grid-col grid-col-4">
						<!-- course item -->
						<div class="course-item">
							<div class="course-hover">
								<img src="../images/h3.jpg" data-at2x="../images/h3.jpg" alt="">
					
							</div>
							
							<div class="course-date bg-color-3 clear-fix">
								
								<div class="divider"></div>
								<div class="description">Financial stability</div>
							</div>
						</div>
						<!-- course item -->
					</div>
				</div>
			</div>
		</section>
		<!-- / section --><br/>
		<hr class="divider-color" />
		<!-- section -->
		<section class="fullwidth-background padding-section" id="health">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<a href="" class="service-icon"><i class="flaticon-pie"></i></a>
						<a href="" class="service-icon"><i class="flaticon-medical"></i></a>
						<a href="" class="service-icon"><i class="flaticon-restaurant"></i></a>
						<a href="" class="service-icon"><i class="flaticon-website"></i></a>
						<a href="" class="service-icon"><i class="flaticon-hotel"></i></a>
						<a href="" class="service-icon"><i class="flaticon-web-programming"></i></a>
						<a href="" class="service-icon"><i class="flaticon-camera"></i></a>
						<a href="" class="service-icon"><i class="flaticon-speech"></i></a>
					</div>
					<div class="grid-col grid-col-6 clear-fix">
						<h3>Health Care</h3>
						<p>The healthcare system is facing unprecedented challenges; with increasing demand from vulnerable patient groups, unhealthy lifestyles, limited capacity of providers, and financial constraints. The current situation has directed the focus of health .</p>
						<h3>Turnkey Solutions</h3>
						<div class="row">
						<a href="" style="font-size: 17px; padding: 5px;">1. Surgical Equipment </a>
						<a href="" style="font-size: 17px; padding: 5px;">2. Medical Gas Pipeline Systems </a>
						<a href="" style="font-size: 17px; padding: 5px;">3. Modular OT</a>
						</div>
						<div class="row">
						<a href="" style="font-size: 17px; padding: 5px;">4. Hospital Fire Fighting Systems </a>
						<a href="" style="font-size: 17px; padding: 5px;">5. Mechanized Laundry Plant </a>
						</div>
						<div class="row">
						<a href="" style="font-size: 17px; padding: 5px;">6. E.T.P  </a>
						<a href="" style="font-size: 17px; padding: 5px;">7. HVAC </a>
						<a href="" style="font-size: 17px; padding: 5px;">8. Telemedicine </a>
						</div>
						<ul>
							
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- / section -->
		<!-- / paralax section -->
		<!-- section -->
		<section class="fullwidth-background padding-section" id="how" style="margin-top: -7em!important;">
			<div class="grid-row">
				<h2 class="center-text">How We Work</h2>
				<!-- time line -->
				<div class="time-line">
					<div class="line-element">
						<div class="action">
							<div class="action-block">
								<span><i class="flaticon-magnifier"></i></span>
								<div class="text">
									<h3>Requirment Analysis</h3>
									<p>For the type of Health e Project & the Scale of Development.</p>
								</div>
							</div>
						</div>
						<div class="level">
							<div class="level-block">Step 1</div>
						</div>
					</div>
					<div class="line-element color-2">
						<div class="level">
							<div class="level-block">Step 2</div>
						</div>
						<div class="action">
							<div class="action-block">
								<span><i class="flaticon-computer"></i></span>
								<div class="text">
									<h3>Fulfillment</h3>
									<p>By Turnkey solutions or by actual construction on site.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="line-element color-3">
						<div class="action">
							<div class="action-block">
								<span><i class="flaticon-shopping"></i></span>
								<div class="text">
									<h3>Maintenance</h3>
									<p>Post development premises maintenace</p>
								</div>
							</div>
						</div>
						<div class="level">
							<div class="level-block">Step 3</div>
						</div>
					</div>
				</div>
				<!-- / time line -->
			</div>
		</section>
		<hr class="divider-color" />
		<section class="fullwidth-background padding-section">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6 clear-fix">
						<img src="../images/h4.jpg" data-at2x="../images/h4.jpg" alt style="width: 95%; border-radius: 20px;">
					</div>
					<div class="grid-col grid-col-6">
						<h2>Our Services</h2>
						<p>Our aim is to ‘add value’ through a systematic approach to project analysis based on our broad experience across the pharmaceutical industry. <br/> <br/>
With a growing and aging population, the health economies around the world are under tremendous pressure to transform for improved care outcomes and patient experiences for significantly less cost.<br/> <br/>

 Motive Consultancy Services innovative approaches and solutions enable this transformation: efficiency and effectiveness; health information exchanges; and patient centricity.<br/> <br/>
</p>
					</div>
				</div>
			</div>
		</section>
		<!-- / paralax section --><br/>
		<hr class="divider-color" />
 <section class="fullwidth-background padding-section" id="about">
			<div class="grid-row clear-fix">
				<h2 class="center-text">About Us</h2>
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<h3>Motive group</h3>
						<p>A FEW WORDS ABOUT Motive Consultancy Services ,  established by Mr. H. S  Sidhu  in 2012 as a business consultancy services with the level of resource associated with larger agencies combined with the flexibility, commitment to customer service and hands-on senior involvement associated with multiple industries. Since then we have been growing from strength to strength and now  office-based researchers and support staff based in Gurugram,  Delhi/NCR.
By employing the brightest researchers and investing in ongoing training we are able to offer an array of services for various Industries  setups ranging from a business developments to a execution of the projects. Motive Group had helped clients to successfully achieved more than 90  projects  in last few years.  <br/><br/>
“A mix of greenfield and brownfield, our projects and clients ranges from a small  scale industries to nation’s largest.” Motive Consultancy Services has set new benchmark in business consultancy servicing domain. 
.</p>

					</div>
					<div class="grid-col grid-col-6">
						<div class="owl-carousel full-width-slider">
							<div class="gallery-item picture">
								<img src="../images/h1.jpg" data-at2x="../images/h1.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/h2.jpg" data-at2x="../images/h2.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/h3.jpg" data-at2x="../images/h3.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/h4.jpg" data-at2x="../images/h4.jpg" alt style="border-radius: 20px;">
							</div>
						</div>
					</div>
				</div>
			</div><br/><br/>
		</section><hr class="divider-color"/>

	<footer id="footer">
		<div class="grid-row">
			<div class="grid-col-row clear-fix">
				<section class="grid-col grid-col-4 footer-about">
					<h2 class="corner-radius">Office</h2>
					
					<address>
						<p></p>
						<a class="phone-number"> 99532 44093</a>
						<br />						<br />
						<a class="email">INFO@MOTIVEGROUP.IN</a>
						<br />						<br />
						<a class="address"> BPH 003 , CENTRAL PARK 1 SEC 42 ,<br/> GOLF COURSE ROAD , GURUGRAM ,<br/> HARYANA, 122011.</a>						<br />
					</address>

				</section>
				<section class="grid-col grid-col-4 footer-latest">
					<h2 class="corner-radius">Services</h2>
					<article>
						<h3>1. Surgical Equipment </h3>
						<h3>2. Medical Gas Pipeline Systems</h3>
						<h3>3. Modular OT</h3>
						<h3>4. Hospital Fire Fighting Systems</h3>
						<h3>5. Mechanized Laundry Plant</h3>
						<h3>6. E.T.P</h3>
						<h3>7. HVAC</h3>
						<h3>8. Telemedicine</h3>
				</section>
				<section class="grid-col grid-col-4 footer-contact-form">
					<h2 class="corner-radius">Contact us</h2>
					<div class="email_server_responce"></div>
					<form action="../form.php" method="POST">
						<?php 
						$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
						<p><span class="your-name"><input type="text" name="name" size="40" placeholder="Name" required></span>
						</p>
						<p><span class="your-email"><input type="email" name="email" size="40" placeholder="Email" required></span> </p>
						<p><span class="your-email"><input type="text" name="phone" size="40" placeholder="Phone" required></span> </p>
						<p><span class="your-message"><textarea name="message" placeholder="Message" cols="20" rows="3" required></textarea></span> </p>
						<input type="hidden" name="redirect" value="<?= $link;?>" />
						<button type="submit" class="cws-button bt-color-3 border-radius alt icon-right">Submit <i class="fa fa-angle-right"></i></button>
					</form>
				</section>
			</div>
		</div>
	</footer>
	<!-- / footer -->
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
	<script type='text/javascript' src='js/jquery.validate.min.js'></script>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/TweenMax.min.js"></script>
	<script src="js/main.js"></script>

	<script src="js/jquery.isotope.min.js"></script>
	
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jflickrfeed.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/retina.min.js"></script>
	<script src="js/jquery.tweet.js"></script>
</body>
</html>