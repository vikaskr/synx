<!DOCTYPE HTML>
<html>
<head>
	<title>Motive Group : We are commited for Better Service</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!-- style -->
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="fi/flaticon.css">
	<link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="css/animate.css">
	<!--styles -->
</head>
<body>
	<!-- page header -->
	<header class="only-color">

		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<!-- logo -->
					<a href="../index.html" class="logo">
						<img src="img/2.jpg" alt="logo" class="logo" style="width: 170px; height: 100px; margin-top: -1em;">
					</a>
					<!-- / logo -->
					<nav class="main-nav">
							<ul class="clear-fix">
							    <li class="static"><a href="../index.html" class="dropdown-toggle" data-toggle="dropdown">Home</a></li>
		                        <li><a class="scrollings active" href="#home">Renewable-Energy</a></li>
		                        <li><a class="scrollings" href="#services" class="scrollings">Services</a></li>
		            
		                        <li><a class="scrollings" href="#about" class="scrollings">About Motive Group</a></li>
		                        <li><a href="#footer" class="scrollings">Contact</a></li>
							</ul>					
					</nav>
				</div>
			</div>
		</div>
		<!-- sticky menu -->
	</header>
					
	
	<div class="">
		<div class="">
			<img src="../images/energy.jpg" data-at2x="../images/energy.jpg" alt style="height: 580px!important; width: 100%!important;">
			<div class="container" style="margin-top: -24em; margin-bottom: 15em;">
				<div class="rows">
					<p style="font-size: 40px; line-height: 50px; padding: 14px; font-weight: 500;">WE ARE COMMITTED <br/>FOR BETTER SERVICES </p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- / revolution slider -->
	<hr class="divider-color">
	<!-- content -->
	<div id="home" class="page-content padding-none">
		<!-- / section -->
		<!-- section -->
		<section class="fullwidth-background padding-section">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<a href="" class="service-icon"><i class="flaticon-pie"></i></a>
						<a href="" class="service-icon"><i class="flaticon-medical"></i></a>
						<a href="" class="service-icon"><i class="flaticon-restaurant"></i></a>
						<a href="" class="service-icon"><i class="flaticon-website"></i></a>
						<a href="" class="service-icon"><i class="flaticon-hotel"></i></a>
						<a href="" class="service-icon"><i class="flaticon-web-programming"></i></a>
						<a href="" class="service-icon"><i class="flaticon-camera"></i></a>
						<a href="" class="service-icon"><i class="flaticon-speech"></i></a>
					</div>
					<div class="grid-col grid-col-6 clear-fix">
						<h2>Renewable Energy</h2>
						<p>DEVELOPMENT	Motive Consultancy ‘s development team supports  clients in the planning, design and construction of both conventional and renewable power generation facilities. Avant is committed to the long-term success of its client’s generation projects. <br/><br/> We don’t just construct state-of-the-art generation, but plan, design and build facilities that will create value over the entire life of the asset.</p>
				</div>
				</div>
			</div>
		</section><br/>
		<!-- / section -->
		<!-- paralax section -->
		<!-- / paralax section -->
		<hr class="divider-color"/>
		<section class="padding-section" id="services">
			<div class="grid-row clear-fix">
				<h2 style="text-align: center; padding: 10px; text-decoration: underline; margin-top: -50px;">Renewable Energy Services</h2>
				<div class="grid-col-row">
					<div class="grid-col grid-col-4 clear-fix">
						<h3>Renewable Energy Advisory </h3>
						<p style="margin-top: 3em;">Our unique Solutions in terms of Engineering Design and Financial Modeling  helps you attain higher margins and better Energy Guarantee! </p>
						<h4>Services We Provide : </h4>
						<ul>
							<li>Advice on sector policy</li>
							<li>Regulatory framework</li>
							<li>Shortlisting the best-suited</li>
						</ul>
					</div>
					<div class="grid-col grid-col-4 clear-fix">
						<h3>Project Management Consultancy </h3>
						<p>At Motive Consultancy Services we believe that every client has a specific need and every project is meant to fulfill that. Hence our solutions are designed keeping a holistic bird's eye view.</p>
						<img src="../images/e8.jpg" data-at2x="../images/e8.jpg" alt style="width: 95%; border-radius: 20px;">
					</div>
					<div class="grid-col grid-col-4 clear-fix">
						<h3>Owner's Engineering/Lender's Engineering</h3>
						<p>We provides expert services to Financial Institutions/ Banks/ Lenders / Private Equity Firms & Hedge Funds during financial closure, Implementation phase, Performance guarantee testing and On Project completion .The services provided broadly include:</p>
						<ul>
							<li>Financial Closure</li>
							<li>Implementation</li>
							<li>Plant Operation</li>
						</ul>
						
					</div>
				</div>
			</div>
		</section><br/>
		<!-- / section -->
		<hr class="divider-color" />
		<!-- section -->
		<section class="padding-section" id="source" style="display: none;">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<img src="../images/e7.jpg" data-at2x="../images/e7.jpg" alt style="width: 95%; border-radius: 20px;">
					</div>
					<div class="grid-col grid-col-6 clear-fix">
						<h2>Learn More About Us From Video</h2>
						<p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at susp. Vivamus orci urna, ornare vitae tellus in, condimentum imperdiet eros. Maecea accumsan, massa nec vulputate congue.</p>
						<p>Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>
						<br/>
					</div>
				</div>
			</div>
		<!-- / section --><hr class="divider-color"/>
		</section>
<section class="fullwidth-background padding-section" id="about">
			<div class="grid-row clear-fix">
				<h2 class="center-text">About Us</h2>
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<h3>Motive group</h3>
						<p>A FEW WORDS ABOUT Motive Consultancy Services ,  established by Mr. H. S  Sidhu  in 2012 as a business consultancy services with the level of resource associated with larger agencies combined with the flexibility, commitment to customer service and hands-on senior involvement associated with multiple industries. Since then we have been growing from strength to strength and now  office-based researchers and support staff based in Gurugram,  Delhi/NCR.
By employing the brightest researchers and investing in ongoing training we are able to offer an array of services for various Industries  setups ranging from a business developments to a execution of the projects. Motive Group had helped clients to successfully achieved more than 90  projects  in last few years.  <br/><br/>
“A mix of greenfield and brownfield, our projects and clients ranges from a small  scale industries to nation’s largest.” Motive Consultancy Services has set new benchmark in business consultancy servicing domain. 
.</p>
						
					</div>
					<div class="grid-col grid-col-6">
						<div class="owl-carousel full-width-slider">
							<div class="gallery-item picture">
								<img src="../images/p18.jpg" data-at2x="../images/p18.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/p19.jpg" data-at2x="../images/p19.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/p20.jpg" data-at2x="../images/p20.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/p17.jpg" data-at2x="../images/p17.jpg" alt style="border-radius: 20px;">
							</div>
						</div>
					</div>
				</div>
			</div><br/><br/>
		</section><hr class="divider-color"/>
	<footer id="footer">
		<div class="grid-row">
			<div class="grid-col-row clear-fix">
				<section class="grid-col grid-col-4 footer-about">
					<h2 class="corner-radius">Office</h2>
					
					<address>
						<p></p>
						<a class="phone-number"> 99532 44093</a>
						<br />						<br />
						<a class="email">INFO@MOTIVEGROUP.IN</a>
						<br />						<br />
						<a class="address"> BPH 003 , CENTRAL PARK 1 SEC 42 ,<br/> GOLF COURSE ROAD , GURUGRAM ,<br/> HARYANA, 122011.</a>						<br />
					</address>
					
				</section>
				<section class="grid-col grid-col-4 footer-latest">
					<h2 class="corner-radius">Services</h2>
					<article>
						<img src="http://placehold.it/83x83" data-at2x="http://placehold.it/83x83" alt>
						<h3>Renewable Energy Advisory</h3>
						<div class="course-date" style="display: none;">
							<div>10<sup>00</sup></div>
							<div>23.02.15</div>
						</div>
						<p>Our unique Solutions in terms of Engineering .</p>
					</article>
					<article>
						<img src="http://placehold.it/83x83" data-at2x="http://placehold.it/83x83" alt>
						<h3>Project Management Consultancy</h3>
						<div class="course-date" style="display: none;">
							<div>10<sup>00</sup></div>
							<div>23.02.15</div>
						</div>
						<p>At Motive Consultancy Services we believe that every client.</p>
					</article>
					<article>
						<img src="http://placehold.it/83x83" data-at2x="http://placehold.it/83x83" alt>
						<h3>Owner's Engineering/Lender's Engineering</h3>
						<div class="course-date" style="display: none;">
							<div>10<sup>00</sup></div>
							<div>23.02.15</div>
						</div>
						<p>We provides expert services to Financial Institutions.</p>
					</article>
				</section>
				<section class="grid-col grid-col-4 footer-contact-form">
					<h2 class="corner-radius">Contact us</h2>
					<div class="email_server_responce"></div>
					<form action="../form.php" method="POST">
						<?php 
						$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
						<p><span class="your-name"><input type="text" name="name" size="40" placeholder="Name" required></span>
						</p>
						<p><span class="your-email"><input type="email" name="email" size="40" placeholder="Email" required></span> </p>
						<p><span class="your-email"><input type="text" name="phone" size="40" placeholder="Phone" required></span> </p>
						<p><span class="your-message"><textarea name="message" placeholder="Message" cols="20" rows="3" required></textarea></span> </p>
						<input type="hidden" name="redirect" value="<?= $link;?>" />
						<button type="submit" class="cws-button bt-color-3 border-radius alt icon-right">Submit <i class="fa fa-angle-right"></i></button>
					</form>
				</section>
			</div>
		</div>
	</footer>
	<!-- / footer -->
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
	<script type='text/javascript' src='js/jquery.validate.min.js'></script>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/TweenMax.min.js"></script>
	<script src="js/main.js"></script>

	<script src="js/jquery.isotope.min.js"></script>
	
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jflickrfeed.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/retina.min.js"></script>
	<script src="js/jquery.tweet.js"></script>
</body>
</html>