<!DOCTYPE HTML>
<html>
<head>
		<title>Motive Group : We are commited for Better Service</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!-- style -->
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="fi/flaticon.css">
	<link rel="stylesheet" href="css/main.css">
	
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="css/animate.css">
	<!--styles -->
</head>
<body>
	<!-- page header -->
	<header class="only-color">
		<!-- header top panel -->
		<!-- / header top panel -->
		<!-- sticky menu -->
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<!-- logo -->
					<a href="../index.html" class="logo">
						<img src="img/2.jpg" alt="logo" class="logo" style="width: 170px; height: 100px; margin-top: -1em;">
					</a>
					<!-- / logo -->
					<nav class="main-nav">
						<ul class="clear-fix">
						    <li class="static"><a href="../index.html" class="dropdown-toggle" data-toggle="dropdown">Home</a></li>
	                        <li><a class="scrollings active" href="#home" class="scrollings">Infrastucture</a></li>
	                        <li><a class="scrollings" href="#services" class="scrollings">Services</a></li>
	                        <li><a class="scrollings" href="#about" class="scrollings">About Motive Group</a></li>
	                        <li><a href="#footer" class="scrollings">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- sticky menu -->
	</header>
	
	<div class="">
		<div class="">
			<img src="../images/3-min.jpg" data-at2x="../images/3-min.jpg" alt style="height: 580px!important; width: 100%!important;">
			<div class="container" style="margin-top: -20em; margin-bottom: 11em;">
				<div class="rows">
					<p style="font-size: 40px; color: #fff; line-height: 50px; padding: 14px; font-weight: 500;">WE ARE COMMITTED <br/>FOR BETTER SERVICES </p>
				</div>
			</div>
		</div>
	</div>
	<!-- / revolution slider -->
	<hr class="divider-color">
	<!-- content -->
	<div id="home" class="page-content padding-none">
		<!-- section -->
		
		<!-- / section -->
		<hr class="divider-color" />
		<!-- section -->
		<section class="fullwidth-background padding-section">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<a href="" class="service-icon"><i class="flaticon-pie"></i></a>
						<a href="" class="service-icon"><i class="flaticon-medical"></i></a>
						<a href="" class="service-icon"><i class="flaticon-restaurant"></i></a>
						<a href="" class="service-icon"><i class="flaticon-website"></i></a>
						<a href="" class="service-icon"><i class="flaticon-hotel"></i></a>
						<a href="" class="service-icon"><i class="flaticon-web-programming"></i></a>
						<a href="" class="service-icon"><i class="flaticon-camera"></i></a>
						<a href="" class="service-icon"><i class="flaticon-speech"></i></a>
					</div>
					<div class="grid-col grid-col-6 clear-fix">
						<h3>Infrastructure</h3>
						<h4>Green Highways for Sustainable Environment & Inclusive Growth</h4>
						<p>"To develop eco friendly National Highways with participation of the community, farmers, NGOs, private sector, institutions, government agencies and the Forest Department for economic growth and development in a sustainable manner."</p>
						
					</div>
				</div>
			</div>
		</section>
		<!-- / section -->
		<!-- paralax section -->
		<div class="parallaxed" id="projects" style="display: none;">
			<div class="parallax-image" data-parallax-left="0.5" data-parallax-top="0.3" data-parallax-scroll-speed="0.5">
				<img src="img/parallax.png" alt="">

			</div>
			<div class="them-mask bg-color-1"></div>
			<div class="grid-row">
				<div class="grid-col-row clear-fix">
					<div class="grid-col grid-col-3 alt">
						<div class="counter-block">
							<i class="flaticon-book1"></i>
							<div class="counter" data-count="356">0</div>
							<div class="counter-name">Courses</div>
						</div>
					</div>
					<div class="grid-col grid-col-3 alt">
						<div class="counter-block">
							<i class="flaticon-multiple"></i>
							<div class="counter" data-count="4781">0</div>
							<div class="counter-name">Students</div>
						</div>							
					</div>
					<div class="grid-col grid-col-3 alt">
						<div class="counter-block">
							<i class="flaticon-pencil"></i>
							<div class="counter" data-count="41">0</div>
							<div class="counter-name">Lections</div>
						</div>
					</div>
					<div class="grid-col grid-col-3 alt">
						<div class="counter-block last">
							<i class="flaticon-calendar"></i>
							<div class="counter" data-count="120">0</div>
							<div class="counter-name">Events</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / section -->
		<hr class="divider-color" />
		<!-- section -->
		<section class="padding-section" id="services">
			<div class="grid-row clear-fix">
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<div class="animated fadeIn active" data-box="1">
								<img src="../images/i12.jpg" data-at2x="../images/i12.jpg" alt style="width: 95%; border-radius: 20px;">
						</div>
					</div>
					<div class="grid-col grid-col-6 clear-fix">
						<h3>Infrastructure Services</h3>
						<h4>Motive Consultancy Services Offers : </h4>
						<div class="row">
						<a href="" style="font-size: 15px; padding: 5px;">1. Advice on sector policy </a>
						<a href="" style="font-size: 15px; padding: 5px;">2. Regulatory framework </a>
						</div>
						<div class="row">
						<a href="" style="font-size: 15px; padding: 5px;">3. Project Management </a>
						<a href="" style="font-size: 15px; padding: 5px; padding-left: 20px;">4. Pavement Management </a>
						</div><br/>
						<h4>Motive Roads & Development Pvt. Ltd.</h4>
						<p>“ Vision: Build World-Class, Mega Organization which makes significant contribution to the Society.”</p>
						<div class="row">
						<a href="" style="font-size: 15px; padding: 5px;">1. Roads & Highways </a>
						<a href="" style="font-size: 15px; padding: 5px;">2. Urban Infrastructure </a>
						<a href="" style="font-size: 15px; padding: 5px;">3. Building & Structure </a>
						</div>
						<div class="row">
						<a href="" style="font-size: 15px; padding: 5px;">4. BOT /DBFO / PPP Projects </a>
						<a href="" style="font-size: 15px; padding: 5px;">5. Traffic and Transportation </a>
						</div>
					</div>
				</div>
			</div>
		</section><br/>
						<br/><hr class="divider-color"/>
		<!-- / section -->
		<section class="fullwidth-background padding-section" id="about">
			<div class="grid-row clear-fix" style="margin-top: -2em;">
				<h2 class="center-text">About Us</h2>
				<div class="grid-col-row">
					<div class="grid-col grid-col-6">
						<h3>Motive group</h3>
						<p>A FEW WORDS ABOUT Motive Consultancy Services ,  established by Mr. H. S  Sidhu  in 2012 as a business consultancy services with the level of resource associated with larger agencies combined with the flexibility, commitment to customer service and hands-on senior involvement associated with multiple industries. Since then we have been growing from strength to strength and now  office-based researchers and support staff based in Gurugram,  Delhi/NCR.
By employing the brightest researchers and investing in ongoing training we are able to offer an array of services for various Industries  setups ranging from a business developments to a execution of the projects. Motive Group had helped clients to successfully achieved more than 90  projects  in last few years.  <br/><br/>
“A mix of greenfield and brownfield, our projects and clients ranges from a small  scale industries to nation’s largest.” Motive Consultancy Services has set new benchmark in business consultancy servicing domain. 
.</p>
						
						<!-- accordions -->
						
						<!--/accordions -->
						
					</div>
					<div class="grid-col grid-col-6">
						<div class="owl-carousel full-width-slider">
							<div class="gallery-item picture">
								<img src="../images/i10.jpg" data-at2x="../images/i10.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/i11.jpg" data-at2x="../images/i11.jpg" alt style="border-radius: 20px;">
							</div>
							<div class="gallery-item picture">
								<img src="../images/i12.jpg" data-at2x="../images/i12.jpg" alt style="border-radius: 20px;">
							</div>
							
						</div>
					</div>
				</div>
			</div><br/><br/>
		</section><hr class="divider-color"/>
	<footer id="footer">
		<div class="grid-row">
			<div class="grid-col-row clear-fix">
				<section class="grid-col grid-col-4 footer-about">
					<h2 class="corner-radius">Office</h2>
					<address>
						<p></p>
						<a class="phone-number"> 99532 44093</a>
						<br />						<br />
						<a class="email">INFO@MOTIVEGROUP.IN</a>
						<br />						<br />
						<a class="address"> BPH 003 , CENTRAL PARK 1 SEC 42 ,<br/> GOLF COURSE ROAD , GURUGRAM ,<br/> HARYANA, 122011.</a>						<br />
					</address>
				</section>
				<section class="grid-col grid-col-4 footer-latest">
					<h2 class="corner-radius">Services</h2>
					<article>
						<h3>1. Advice on sector policy </h3>
						<h3>2. Regulatory framework </h3>
						<h3>3. Project Management </h3>
						<h3>4. Pavement Management </h3>
						<h3>5. Roads & Highways</h3>
						<h3>6. Urban Infrastructure</h3>
						<h3>7. Building & Structure</h3>
						<h3>8. BOT /DBFO / PPP Projects</h3>
						<h3>9. Traffic and Transportation</h3>
					</article>
					
				</section>
				<section class="grid-col grid-col-4 footer-contact-form">
					<h2 class="corner-radius">Contact us</h2>
					<div class="email_server_responce"></div>
					<form action="../form.php" method="POST">
						<?php 
						$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
						<p><span class="your-name"><input type="text" name="name" size="40" placeholder="Name" required></span>
						</p>
						<p><span class="your-email"><input type="email" name="email" size="40" placeholder="Email" required></span> </p>
						<p><span class="your-email"><input type="text" name="phone" size="40" placeholder="Phone" required></span> </p>
						<p><span class="your-message"><textarea name="message" placeholder="Message" cols="20" rows="3" required></textarea></span> </p>
						<input type="hidden" name="redirect" value="<?= $link;?>" />
						<button type="submit" class="cws-button bt-color-3 border-radius alt icon-right">Submit <i class="fa fa-angle-right"></i></button>
					</form>
				</section>
			</div>
		</div>
	</footer>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
	<script type='text/javascript' src='js/jquery.validate.min.js'></script>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/TweenMax.min.js"></script>
	<script src="js/main.js"></script>	
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jflickrfeed.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/retina.min.js"></script>
	<script src="js/jquery.tweet.js"></script>
</body>
</html>