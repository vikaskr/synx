
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "motive";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


    if(isset($_POST['name']))
    {
        $name = @trim(stripslashes($_POST['name'])); 
        $email = @trim(stripslashes($_POST['email'])); 
        $phone = @trim(stripslashes($_POST['phone'])); 
        $message = @trim(stripslashes($_POST['message'])); 

        $sql = "INSERT INTO contact (name,email,phone,message) VALUES ('$name','$email','$phone','$message')";

        if ($conn->query($sql) === TRUE) {
            echo "";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
     }
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="theme-color" content="#0642a3"/> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>Motive Group : We are commited for Better Service</title>

<!-- main styling file  -->
<link rel="stylesheet" href="css/site.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/typo1.css">
<link rel="icon" href="images/favicon.png">
<style type="text/css">
.green-contact-form {
  width: 700px;
  margin:0 auto;
  border-radius:10px;
}

.title-form {
  max-width: 550px;
}

</style>
</head>
<body data-spy="scroll" data-target=".scrolly" data-offset="50">


<div class="wrapper">

<!-- Main-Navigation -->
<header id="main-navigation">
   <div id="navigation" class="darkdropdown">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <ul class="top-right text-right">
                  <li><a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="javascript:void(0)" class="twitter"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="javascript:void(0)" class="instagram"><i class="fa fa-instagram"></i></a></li>
               </ul>
               <nav class="navbar navbar-default">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="true"> 
                         <span class="icon-bar top-bar"></span> 
                         <span class="icon-bar middle-bar"></span> 
                         <span class="icon-bar bottom-bar"></span> 
                     </button>
                     <a class="navbar-brand" href="index.html">
                       <img src="images/2.jpg" alt="logo" class="logo" style="width: 170px; height: 100px; margin-top: -1em;">
                       <img src="images/2.jpg" alt="logo" class="scrolled-logo">
                      </a>
                  </div>
                  <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right scrolly">
                     <ul class="nav navbar-nav">
                        <li><a class="scrollings" href="index.html">Home</a></li>
                        <li><a class="scrollings" href="/health/health.php">Health-Care</a></li>
                        <li><a class="scrollings" href="/energy/energy.php">Renewable-Energy</a></li>
                        <li><a class="scrollings" href="/infrastucture/infrastucture.php" class="scrollings">Infrastucture</a></li>
                        <li><a class="scrollings" href="/pmc/pmc.php" class="scrollings">Project PMC</a></li>
                        <li><a class="scrollings" href="/finance/finance.php" class="scrollings">Financial</a></li>
                        <li><a class="scrollings" href="contact.php" class="scrollings">Contact</a></li>
                     </ul>
                  </div>
               </nav>
            </div>
         </div>
      </div>
   </div>
</header>
<!-- Main-Navigation ends -->
          

<!--Single portfolio items-->
<section class="single-items center-block center-block item-one parallaxie full-screen">
   <div class="container">
      <div class="row">
         <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-11">
            
         </div>
      </div>
   </div>
</section>
<section style="background-color: #f9ecec; padding: 2em; min-height: 600px;">
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <div class="green-contact-form p-5 bg-white">
            <div class="title-form mb-5">
              <h3 style="font-size: 40px;">Get in touch</h3>
              <p>We’re very approachable and would love to speak to you. Feel free to call, send us an email, simply complete the enquiry form.</p>
            </div>
            
            <div class="form-box">
              <h4>Send Us a Message</h4><br/>
                  <form name="contact-form" method="POST" action="contact.php">
                    <fieldset class="form-group">
                      <label for="name">Your Name (required)</label>
                      <input type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="email">Email Adressess (required)</label>
                      <input type="email" class="form-control" name="email" placeholder="Enter Your Email" required>
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="phone">Mobile Number (required)</label>
                      <input type="text" class="form-control" name="phone" placeholder="Enter Your Phone" required="">
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="message">Your message (required)</label>
                      <textarea name="message" class="form-control" rows="2" required></textarea>
                    </fieldset>
                    <button type="submit" class="btn btn-success" required>SEND MESSAGE</button>
                  </form>
            </div>
          </div>
         </div>

          <div class="col-md-4">
            <h3 style=" font-size:35px; margin-top: 1em;margin-bottom: 2em; text-decoration: underline;">Office Address :</h3>
            <h4 style="line-height: 24px;"><img src="images/map.png" height="25"> BPH 003 , CENTRAL PARK 1 SEC 42 ,<br/> GOLF COURSE ROAD , GURUGRAM ,<br/> HARYANA, 122011.<br/><br/>
            <img src="images/email.png" height="25"> EMAIL : INFO@MOTIVEGROUP.IN<br/><br/>

            <img src="images/phone.png" height="25">  PHONE : 99532 44093<br/></h4>
          </div>
      </div>
   </div>
</section>


<footer class="padding_half">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <ul class="social wow BounceIn">
               <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
               <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
               <li><a href="javascript:void(0)"><i class="fa fa-dribbble"></i> </a> </li>
               <li><a href="javascript:void(0)"><i class="fa fa-flickr"></i> </a> </li>
            </ul>
            <p class="darkcolor top30 wow fadeInUp">Copyright &copy; 2018 MakeUBIG. all rights reserved.</p>
         </div>
      </div>
   </div>
</footer>
<!--Footer ends-->
</div>


<!-- jQuery Files -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--to view items on reach-->
<script src="js/jquery.appear.js"></script>

<!--Swiper slider-->
<script src="js/swiper.jquery.min.js"></script>

<!--Owl slider-->
<script src="js/owl.carousel.min.js"></script>

<!--number counters-->
<script src="js/jquery-countTo.js"></script>

<!--equalize the same heights of block-->
<script src="js/jquery.matchHeight-min.js"></script>

<!--for parallax bgs-->
<script src="js/parallaxie.js"></script>

<!--for CountDown Timer-->
<script src="js/dscountdown.min.js"></script>

<!--Open popup fancybox on images-->
<script src="js/jquery.fancybox.min.js"></script>

<!--Portfolio galleries-->
<script src="js/jquery.cubeportfolio.min.js"></script>

<!--Progressbar s in circle forms-->
<script src="js/circle-progress.min.js"></script>

<!--scrollbar on blocks-->
<script src="js/simplebar.js"></script>

<!--Video Pops support for youtube, viemo etc-->
<script src="js/viedobox_video.js"></script>

<!--youtube background video-->
<script src="js/jquery.mb.YTPlayer.min.js"></script>

<!-- Type It -->
<script src="https://cdn.jsdelivr.net/jquery.typeit/4.4.0/typeit.min.js"></script>

<!-- WOW Transitions -->
<script src="js/wow.min.js"></script>
    
<!--Revolution SLider-->
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="js/revolution.extension.actions.min.js"></script>
<script src="js/revolution.extension.carousel.min.js"></script>
<script src="js/revolution.extension.kenburn.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.migration.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
   

<!--Synx scripts-->
<script src="js/functions.js"></script> 

</body>
</html>